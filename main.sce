//Nom du script : main.sce
//Date de création : 08/11/2016
//Auteur : Romain Bosquet

//************ Suivi des modifications
// reprise de main_volume_coline fait pendant la thèse

//************ Présentation générale du script
// main de l'optimsation



//******** Ordi perso
rep_commun='/home/bosquet/Documents_R/2016_Transpod/Scilab/';

//répertoir de travail :
chdir(rep_commun+'Scilab_2/');

dt_deb=getdate();
disp(msprintf('Execution le %i/%i/%i à %i:%i:%i ',dt_deb(6),dt_deb(2),dt_deb(1),dt_deb(7),dt_deb(8),dt_deb(9) ) );



//***************** fonction permettant de trouver des solutions initiales
exec('function_solutions_ini.sce');
//***************** fonction calcul tracé de la voie (à partir des paramètres)
exec('function_trace_voie.sce');
//***************** fonction calcul cout de la voie (à partir du tracé)
exec('function_calcul_cout.sce');
//***************** fonction  haut niveau
exec('functions_haut_niveau_cout_optim.sce');


p_mut=0.3;                  //the mutation probability (default value: 0.1).  
p_cross=0.5;                //the crossover probability (default value: 0.7) ->qd on mélange
Log=%t;                     //if %T, we will display to information message during the run of the genetic algorithm

ga_params = init_param();
// Parameters to adapt to the shape of the optimization problem
ga_params = add_param(ga_params,"minbound",borne_inf);
ga_params = add_param(ga_params,"maxbound",borne_sup);
ga_params = add_param(ga_params,"dimension",length(x0));
// définit la fonction : function Pop_init = init_ga_default_perso(popsize,param)
ga_params = add_param(ga_params,"init_func",init_route_spline);     //init_route_spline
                                            
disp(msprintf('Pop size : %i, nb gene : %i, nb para : %i, p_mu : %i, p_cross : %i',pop_size,nb_generation,length(x0),p_mut*100,p_cross*100));

//on netttoye l'environnement
    //optimisation
//clear borne_inf borne_sup x0
    //autre variables
clear xy_deb_voie xy_fin_voie pas_dis_voie alti_deb_voie alti_deb_fin id_m

[pop_opt,fobj_pop_opt,pop_init,fobj_pop_init] = optim_ga(function_cout_voie_optim,pop_size,nb_generation,p_mut,p_cross,Log,ga_params);
//  pop_opt         the population of optimal individuals.
//  fobj_pop_opt    the set of objective function values associated to pop_opt (optional).
//  pop_init        the initial population of individuals (optional).
//  fobj_pop_init   the set of objective function values associated to pop_init (optional).


dt_fin=getdate();
disp(msprintf('Fin execution le %i/%i/%i à %i:%i:%i ',dt_fin(6),dt_fin(2),dt_fin(1),dt_fin(7),dt_fin(8),dt_fin(9) ) );

duree_exe = etime(dt_fin,dt_deb);   //en seconde
duree_exe_j=floor(duree_exe/ (60*60*24) );     //en jours
duree_exe_h= floor( ( duree_exe - duree_exe_j*60*60*24 )/ (60*60) ) ;  //en heures
duree_exe_m= floor( (duree_exe - duree_exe_j*60*60*24  - duree_exe_h*60*60 )/60 ) ;  //en minutes
disp(msprintf('Durée execution : %i jour(s), %i heure(s), %i minute(s) !',duree_exe_j,duree_exe_h,duree_exe_m ) );

//backup of the optimal route and parameters
save(msprintf('backup_simu_pop_size_%i_nb_gene_%i_p_mu_%i_p_cross_%i',pop_size,nb_generation,p_mut*100,p_cross*100),'pop_opt','fobj_pop_opt','pop_init','fobj_pop_init','duree_exe','pop_size','nb_generation','p_mut','p_cross');
