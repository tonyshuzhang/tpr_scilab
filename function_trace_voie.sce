//Nom de la fonction : function_calcul_trace_voie
//Date de création : 02/02/2017
//Auteur : Romain Bosquet

//************ Suivi des modifications


//************ Présentation générale de la fonction
//

//************ Paramètres
//      - parametre de la voie : "parametre", dans l'interval [-1 1]
//      - option de graph      : "plot_2d"      TRUE ou FALSE (graphique ou non)

//************ Sorties
//      coordonnées de la voie dans le repère "corner" (si (0,0) dans l'angle xycorner
//      abscisse curviligne, rayon, courbure et déclivité (en m, m, 1/m, et m/m)



function [coordonnees_voie,abs_R_C_D]=function_trace_voie(parametre,plot_2d)

//***********************************************************************************************
//*****************************         DEPART ARRIVEE           ********************************

    //dans le cas Toronto
            //ncols        6306
            //nrows        4412
            //xllcorner    1193998.768016045215
            //yllcorner    -459329.836859913834
            //cellsize     90.000000000000
            //NODATA_value -32767
    xycorner=[1193998.768016045215 -459329.836859913834];
            //=> ce vecteur définit l'angle des données exportées par QGIS


    //********************************************************************* 
    //************************* départ/arrivée
    
    //cf : http://umap.openstreetmap.fr/en/map/transpod-toroton-montreal-route-design_110783
    xy_Toronto_3979=[1273278.28 -423151.27];            //=> coordonnées en lambert 3979
    xy_Toronto=xy_Toronto_3979-xycorner;

    xy_Peterborough_3979=[1345849.87 -354225.69];       //=> coordonnées en lambert 3979
    xy_Peterborough=xy_Peterborough_3979-xycorner;
    
    xy_Otawa_3979=[1519294.35 -178990.14];              //=> coordonnées en lambert 3979
    xy_Otawa=xy_Otawa_3979-xycorner;
    
    //********** a modifier en fonction du point d'arrivée et de départ souhaité !
    xyz_depart=xy_Peterborough;
    xyz_arrive=xy_Otawa;
            
            //simu 1 : Toroton / Peterborough
            //simu 2 : Peterborough / Otawa
    
    
    //************************* tracé
    //éloignement que l'on accepte de la ligne droite en départ et arrivée :
    diff_delta_xy_m=10000;  //en m
    
    //on applique également un delta de coeff d'altitude
    diff_delta_z_max=300;   //en m
    
    //la voie va ensuite être interpolées
    nb_points_interpolation=500;
 
//***********************************************************************************************
//***************************** CALCUL ALTITUDE DEBUT ET FIN VOIE *******************************
    
    load('terrain_ligne_ToMo','abscisse_x','abscisse_y','profil_Toronto');
            //size(abscisse_x)      =   1.    1385.  
            //size(abscisse_y)      =   1.    959.  
            //size(profil_Toronto)  = 1385.    959. 
    
    x_terrain=abscisse_x';  //du coup, x_terrain vecteur colonne
    y_terrain=abscisse_y';  //du coup, y_terrain vecteur colonne
    z_terrain=profil_Toronto;   //z_terrain matrice
    clear abscisse_x abscisse_y profil_Toronto

    //on cherche l'altitude du terrain au début et à la fin de la voie par interpolation
    zp = linear_interpn([xyz_depart(1); xyz_arrive(1)],[xyz_depart(2); xyz_arrive(2)], x_terrain, y_terrain, z_terrain);

    xyz_depart(3)=zp(1);
    xyz_arrive(3)=zp(2);

    clear x_terrain y_terrain z_terrain zp

//***********************************************************************************************
//************************************* CHANGEMENT DE REPERE ************************************

    //Changement de repère pour commencer de sorte que :
    //                      -   y départ = y arrivée = 0
    //                      -     départ = 0 0 0

    //calcul de l'angle de rotation pour le changement de repère 
    angle=atan( xyz_arrive(2)-xyz_depart(2) , xyz_arrive(1)-xyz_depart(1) );
    
    //matrice de rotation pour avoir y d = y a
    M_r=[ cos(angle) -sin(angle) 0 ; sin(angle) cos(angle) 0 ; 0 0 1 ];
    
    //coordonnées point départ  => c'est bien un vecteur null
    //xyz_d=(xyz_depart-xyz_depart)*M_r
    xyz_d=[0    0      0];
    
    //coordonnées point arrivée, de la forme xyz_a=[a    0        b];
    xyz_a=(xyz_arrive-xyz_depart)*M_r;
    
    //Matrice de rotatio inverse (pour retrouver le repère d'origine) :
    M_r2=[ cos(-angle) -sin(-angle) 0 ; sin(-angle) cos(-angle) 0 ; 0 0 1 ];


//************************************************************************************************
//************************************* POINTS DE CONTRAINTES ************************************
    //nb point que l'on veut fixer entre départ et arrivée
    nb_contraintes=length(parametre)/2;
    
    if plot_2d then
        figure();
        //plot des 2 points départ et arrivée :
        plot2d([xyz_d(1) xyz_a(1)],[xyz_d(2) xyz_a(2)],-1)
        //plot d'une droite entre les 2 points
        plot2d([xyz_d(1) xyz_a(1)],[xyz_d(2) xyz_a(2)],3)
    end
    
    //      on définit des points de contraintes (dans un premier temps équidistant entre départ et arrivée, sur la ligne droite)
    for i=1:nb_contraintes
        xyz_c(i,:)=xyz_d+ ( (xyz_a-xyz_d)./(nb_contraintes+1) ).*i;     //on divise par nb_contrainte+1 car sinon la contrainte finale serait le point d'arrivée
             //delta entre arrivée et départ : (xyz_a-xyz_d)
    end
    
    //dans le plan xy   vecteur entre départ et arrivée : 
    v=(xyz_a-xyz_d);
    //vecteur orthogonal (normalisé)
    vn_xy=[-v(2) v(1) 0]/( sqrt( v(1)^2+v(2)^2) );
    //disp('vn_xy, doit être 0 1 0 !!');disp(vn_xy);
        //=> si on a fait notre changement de repère, vn_xy=0 1 
    vn_xy=[0 1 0];
    
    //      paramètres en xy, interval [-1 1] : 
    para_xy=parametre(1:nb_contraintes);
    //para_xy est un vecteur colonne !
    
    if length(para_xy)<>nb_contraintes then
        erreur
    end
    
    if plot_2d then
        //affichage des points avant éloignement de la droite
        plot2d(xyz_c(:,1),xyz_c(:,2),-3);
    end

    //      les points de contraintes sont éloignées de la droite départ/arrivée via le vecteur normal
    xyz_c(:,:)=xyz_c(:,:)+diff_delta_xy_m*para_xy*vn_xy;
    
    if plot_2d then
        //affichage des nouveaux points de contraintes
        plot2d(xyz_c(:,1),xyz_c(:,2),-3);
        xtitle("visu des points de contraintes dans le plan x y");
    end
    
    //      paramètres en z, interval [-1 1]
    para_z=parametre(nb_contraintes+1:$);
    //para_z est un vecteur colonne
    
    //      l'altitude des points de contraintes est modifiées
    xyz_c(:,3)=xyz_c(:,3)+diff_delta_z_max*para_z;
    
    
    //  *********** contraintes (coordonnées des points)

    //on trie les points de contraintes en x
    xyz_c_sort=gsort(xyz_c,'lr','i');
    if sum(xyz_c_sort<>xyz_c)<>0 then
        Erreur, ce n'est pas normal, x doit déjà être croissant !
        en effet, si on a bien le changement de repère avec y départ = y arrivée, le delta xy se fait uniquement sur y !
    end
    clear xyz_c_sort
    
    //on ajoute les contraintes départ et arrivée :
    xyz_c(nb_contraintes+1,:)=xyz_d;
    xyz_c(nb_contraintes+2,:)=xyz_a;
    
    //on tri pour avoir selon x croissant (suite à l'ajout de départ et arrivée)
    xyz_c=gsort(xyz_c,'lr','i');
    
    x = xyz_c(:,1);
    y = xyz_c(:,2);
    z = xyz_c(:,3);
    
    //x = [0 ; 0.2 ; 0.4 ; 0.6 ; 0.8 ;  1];
    //y = [0 ; 0.1 ;-0.1  ; 0  ;-0.1 ;  0];
    //z = [0 ; 0.2 ; 0.4  ;0.6 ; 0.8 ;  1];
    
    //attention, si x n'est pas strictement croissant, on va avoir un bug ! (=> en théorie, pas possible !)
    if min(abs(diff(x)))==0 then
        erreur ! x n'est pas strictement croissant pour les différents points !
    end


//****************************************************************************************
//************************************* INTERPOLATION ************************************
    //vecteur interpolation en x
    xx = linspace(min(x), max(x), nb_points_interpolation)';
    
    yk = interp(xx, x, y, splin(x,y,"not_a_knot"));
    zk = interp(xx, x, z, splin(x,z,"not_a_knot"));
    
    yf = interp(xx, x, y, splin(x,y,"fast"));
    zf = interp(xx, x, z, splin(x,z,"fast"));
    
    ym = interp(xx, x, y, splin(x,y,"monotone"));
    zm = interp(xx, x, z, splin(x,z,"monotone"));
    
    if plot_2d then
        figure()
        plot2d(xx, [yf ym yk], style=[5 2 3], strf="121", leg="fast@monotone@not a knot spline")
        plot2d(x,y,-9, strf="000")  // to show interpolation points
        xtitle("visu voie dans le plan x y");
        show_window()
        
        figure()
        plot2d(xx, [zf zm zk], style=[5 2 3], strf="121", leg="fast@monotone@not a knot spline")
        plot2d(x,z,-9, strf="000")  // to show interpolation points
        xtitle("visu voie dans le plan x z");
    end
    
    //choix de l'interpolation
    yy=ym;
    zz=zm;

    clear yk zk yf zf ym zm

//*****************************************************************************************
//********************************* CALCUL : **********************************************
//*********************** RAYON, COURBRUE, ABSCISSE CURVILIGNE, DECLIVITE *****************
    
    //on calcul l'abscisse curviligne
    s=[0 ; sqrt( diff(xx).^2 + diff(yy).^2 + diff(zz).^2) ];
    ss=cumsum(s);
    
    // d'après : http://www.bibmath.net/dico/index.php?action=affiche&quoi=./c/courbure.html ou https://fr.wikipedia.org/wiki/Rayon_de_courbure
    // le rayon de courbure :
    //  R = ( ( x'2 + y'2)^(3/2) ) / ( y''*x' - x''y' )
    //          ATTENTION, les dérivées sont en fonction du temps ! x' = dx/dt
    
    //  ************************* dy/dt
    for i=3:length(yy)
        ydt1(i-1)=(yy(i)-yy(i-2))/ 2;
    end
        ydt1(length(yy))=0;
    
    //  ************************* d2y/dt2
    for i=3:length(yy)
        ydt2(i-1)=(ydt1(i)-ydt1(i-2))/ 2;
    end
        ydt2(length(yy))=0;
    
    //  ************************* dx/dt
    for i=3:length(xx)
        xdt1(i-1)=(xx(i)-xx(i-2))/ 2;
    end
        xdt1(length(xx))=0;
    
    //  ************************* d2x/dt2
    for i=3:length(xx)
        xdt2(i-1)=(xdt1(i)-xdt1(i-2))/ 2;
    end
        xdt2(length(xx))=0;
    
    
    //Rayon de coubure
    //attention, on vérifie que l'on n'a pas de devision par 0
    if ( sum( ( xdt1(2:$-1).*ydt2(2:$-1) - xdt2(2:$-1).*ydt1(2:$-1) )== 0 )>0 ) then
        //si divison par zéro, Rayon = vecteur null
        R=zeros(length(xdt1),1);
        disp('Attention, division par zéro pour calcul du R');
    else
        R= ( ( xdt1(2:$-1).^2 + ydt1(2:$-1).^2 ).^(3/2) ). / ( xdt1(2:$-1).*ydt2(2:$-1) - xdt2(2:$-1).*ydt1(2:$-1) );
        R(1)=0;
        R(length(xdt1))=0;
    end

    //ou courbure (=1/R)
    C= (xdt1(2:$-1).*ydt2(2:$-1) - xdt2(2:$-1).*ydt1(2:$-1)). / ( ( xdt1(2:$-1).^2 + ydt1(2:$-1).^2 ).^(3/2) );
    C(1)=0;
    C(length(xdt1))=0;

    //déclivité
    //  ************************* dz/ds
    for i=3:length(zz)
        zds1(i-1)=( zz(i)-zz(i-2) ) / ( ss(i) - ss(i-2) );
    end
        zds1(length(zz))=0;

    
    if plot_2d then
        figure();
        plot2d(ss,R);
        xtitle("rayon de courbure fonction abscisse curviligne (m)");
        
        figure();
        plot2d(ss,C);
        xtitle("courbure fonction abscisse curviligne (1/m)");
        
        figure();
        plot2d(ss,zds1);
        xtitle("déclivité fonction abscisse curviligne (m/m)");
    end
    
    
//***********************************************************************************************
//************************************* CHANGEMENT DE REPERE ************************************
    //on veut visualiser dans le plan d'origine la voie
    //les coordonnées de la voie : xx yy zz
    
    coordonnees_voie=[xx yy zz]*M_r2+ones(length(xx),3)*diag(xyz_depart);
    
    if plot_2d then
        figure();
        plot2d(coordonnees_voie(:,1),coordonnees_voie(:,2));
        plot2d([xyz_depart(1) xyz_arrive(1)],[xyz_depart(2) xyz_arrive(2)],-3);
        xtitle("visu voie dans le repère d''orgine en x y");
        
        figure();
        plot2d(coordonnees_voie(:,1),coordonnees_voie(:,3));
        plot2d([xyz_depart(1) xyz_arrive(1)],[xyz_depart(3) xyz_arrive(3)],-3);
        xtitle("visu voie dans le repère d''orgine en x z");
        
    end
    
    abs_R_C_D=[ss R C zds1];        //en m, en m, en 1/m, en m/m
    
endfunction
