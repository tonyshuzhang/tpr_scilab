//Nom de la fonction : init_route_spline
//Date de création : 31/01/2017
//Auteur : Romain Bosquet

//************ Suivi des modifications
//  8/11/2016 : adaptation pour projet transpod


//************ Présentation générale de la fonction
// Cette fonction génère de la manière aléatoire des solutions initiales


//************ Sorties
//      population initiale sous forme de liste
//      les paramètres sont dans l'interval [-1 1]

//************ Paramètres
//les paramètres que cette fonction utilise :
// * nb de paramètres
// * nb de la population initiale


function Pop_init = init_route_spline(popsize,param)
	disp('Debut fonction init_route_spline');

	// Pop_init must be a list()
	Pop_init = list();
    disp(param);

	for sol_i=1:popsize
        Pop_init(sol_i)=2*rand(param.dimension,1)-1;	//rand entre 0 et 1
    end

	disp('Fin de fonction init_route_spline');    

    //a priori, Pop init soit être une liste de vecteur colonne
endfunction
