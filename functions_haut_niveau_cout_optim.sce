//Nom de la fonction : functions_haut_niveau_cout_optim.sce
//Date de création : 02/02/2017
//Auteur : Romain Bosquet

//************ Suivi des modifications


//************ Présentation générale de la fonction
//

//************ Sorties
// cout de construction de la voie

//************ Paramètres
//  paramètres de la voie - dans l'interval [-1 1]
//  



//**************************************** Construction + evalue cout solution ****************************************
//****************************************                                     ****************************************
//paramètres d'entrées : 
//              - les paramètres 
//paramètre de sortie :
//              - cout de la voie
function [cout_construc]=function_cout_voie_optim(parametres)

    //on trace la voie
    [coordonnees_voie,abs_R_C_D]=function_trace_voie(parametres,%f);    //%f en 2ième paramètre car on ne veut pas de tracé !
    
    //on calcul son cout
    [cout_construc,trace_xyzz]=function_cout_voie(coordonnees_voie,abs_R_C_D,%f);      //%f en 3ième paramètre car on ne veut pas de tracé !
    
    //on sauvegarde dans un fichier la voie :
    //save( msprintf('voies_testees/cout_voie_%i',cout_construc), 'coordonnees_voie','abs_R_C_D','cout_construc');
    
endfunction


