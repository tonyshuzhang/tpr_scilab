//Nom de la fonction : function_calcul_cout_voie
//Date de création : 02/02/2017
//Auteur : Romain Bosquet

//************ Suivi des modifications


//************ Présentation générale de la fonction
// Cette fonction calcul le cout d'une voie

//************ Paramètres
//

//************ Sorties
//      - figure(51)    altitude de la voie et du sol en fonction de l'abscisse curviligne
//      - 


function [cout_construc,trace_xyzz]=function_cout_voie(trace_xyz,abs_R_C_D,plot_2d)

    load('terrain_ligne_ToMo','abscisse_x','abscisse_y','profil_Toronto');
    //size(abscisse_x) =   1.    1385.  
    //size(abscisse_y) =   1.    959.  
    //size(profil_Toronto) = 1385.    959. 

    x_terrain=abscisse_x';  //du coup, x_terrain vecteur colonne
    y_terrain=abscisse_y';  //du coup, y_terrain vecteur colonne
    z_terrain=profil_Toronto;   //z_terrain matrice

    clear abscisse_x abscisse_y profil_Toronto

    //alitude du terrain au point de coordonnées x y de la voie
    zp = linear_interpn(trace_xyz(:,1),trace_xyz(:,2), x_terrain, y_terrain, z_terrain);
    
    //différence altitude entre le sol et la voie
    diff_alti_voie_sol=abs(zp-trace_xyz(:,3));
    
    //cout de construction : intégrale entre diff voie et sol en fonction abscisse curviligne
    diff_absci_cur=diff(abs_R_C_D(:,1));
    diff_absci_cur=diff_absci_cur';
    cout_construc=diff_absci_cur*diff_alti_voie_sol(2:$);

    if plot_2d then
        scf(51);
        plot2d(abs_R_C_D(:,1),[trace_xyz(:,3) zp], style=[2 3], leg="voie@sol")
        xtitle("altitude voie et sol dans le plan z abscisse curviligne");
        
        scf(52);
        plot2d(trace_xyz(:,1),trace_xyz(:,2))
        xtitle("Tracé de la voie dans le plan x y");
    end

    trace_xyzz=[trace_xyz zp];

endfunction
